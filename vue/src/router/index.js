//import vue router
import { createRouter, createWebHistory } from 'vue-router'

//define a routes
const routes = [
    {
        path: '/',
        name: 'authentication.login',
        component: () => import('@/components/authentication/Login.vue')
    },
    {
        path: '/register',
        name: 'authentication.register',
        component: () => import('@/components/authentication/Register.vue')
    },
    {
        path: '/home',
        name: 'dashboard.home',
        component: () => import('@/components/dashboard/home.vue')
    }
    // {
    //     path: '/edit/:id',
    //     name: 'post.edit',
    //     component: () => import( /* webpackChunkName: "post.edit" */ '@/views/post/Edit.vue')
    // }
]

//create router
const router = createRouter({
    history: createWebHistory(),
    routes  // config routes
})

export default router